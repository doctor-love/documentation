---
title: System Transparency
weight: 1
---

# Welcome

The System Transparency project provides a variety of components that can be used together for secure/trustworthy data center operations. Several of these are optional and can be excluded/replaced depending on your preferences, requirements and threat-model. The cornerstone of System Transparency and only mandatory component is the bootloader, which is signed with a key that is embedded in the firmware of your server. This ensures that the bootloader is not tampered with and can be launched on any UEFI Secure Boot enabled platform.
