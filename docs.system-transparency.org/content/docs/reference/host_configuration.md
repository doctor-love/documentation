---
title: Host Configuration
---

# Host Configuration

The host configuration for stboot contains a collection of settings in JSON
format. it contains options that are host-specific. It is used by stboot and
the OS package to correctly configure the network.

## Source

The configuration file is auto-detected according to the following order:

1) `/etc/host_configuration.json`
2) EFI variable `STHostConfig` with GUID `f401f2c1-b005-4be0-8cee-f2e5945bcbe7`

## Host Configuration Format

The trust policy settings are represented as a JSON object:

The network configuration, including binding interfaces, will be applied by
stboot before downloading the OS package. Failure cause a restart.

See the [JSON Schema definitions](/host_configuration.schema.json) of for the
exact format of a host configuration. A overview can be found in the table
below.

| Field              | Type         | Required if              | Description                                                   |
|--------------------|--------------|--------------------------|---------------------------------------------------------------|
| network_mode       | string       | *always*                 | Select between dynamic IP address via DHCP or static.         |
| host_ip            | string       | network_mode == "static" | Static IP address to assign to all interface.                 |
| gateway            | string       | network_mode == "static" | Default gateway to configure.                                 |
| dns                | string array | network_mode == "static" | List DNS servers to use.                                      |
| network_interfaces | string array | *always*                 | Network interfaces to configure and to bond.                  |
| ospkg_pointer      | string       | *always*                 | Location of the OS package. Either a URL or a path.           |
| identity           | string       | *always*                 | Replaces all occurrences of $ID in the ospkg_pointer field.   |
| authentication     | string       | *always*                 | Replaces all occurrences of $AUTH in the ospkg_pointer field. |
| bonding_mode       | string       | network_interfaces > 1   | Bonding mode to use for combining network_interfaces.         |
| bond_name          | string       | network_interfaces > 1   | Name of the bonded interface if bonding_mode is not null.     |


_Example_
```json
{
  "network_mode":"dhcp",
  "host_ip":null,
  "gateway":null,
  "dns":null,
  "network_interfaces":null,
  "ospkg_pointer": "http://10.0.2.2:8080/os-pkg-example-ubuntu20.json",
  "identity":null,
  "authentication":null,
  "bonding_mode": "",
  "bond_name": ""
}
```
