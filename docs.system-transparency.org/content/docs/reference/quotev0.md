---
title: quotev0
math: true
---

# quotev0

quotev0 defines the [protocol
messages](https://git.glasklar.is/system-transparency/core/stauth/-/blob/main/quotev0/quotev0.proto)
used to request and deliver a quote from the device to the operator. The quote
protocol only needs a single round trip to request and receive a fresh quote.

For detailed definitions of the TPM commands and structures used in this
document, refer to parts two and three of the [Trusted Platform Module Library
Specification](https://trustedcomputinggroup.org/resource/tpm-library-specification/)
published by the Trusted Computing Group.

The figure below shows all interactions of the `quotev0` protocol. The protocol
only standardizes the messages between the operator and the device to be
quoted. For clarity the figure below also includes the commands send to the
TPM.


```mermaid
sequenceDiagram
    autonumber
    participant O as stauth on Operator
    participant D as stauth on Device
    participant TPM

    O->>D: AIK, Nonce, PCR Indices

    activate D
    D->>TPM: TPM2_CreatePrimary
    D->>TPM: TPM2_Load(AIK)

    D->>TPM: TPM2_ReadPCR(PCR Indices)
    activate TPM
    TPM->>D: PCR Values
    deactivate TPM

    D->>TPM: TPM2_Quote(Nonce)
    activate TPM
    TPM->>D: Quote, Signature
    deactivate TPM

    D->>O: Quote, Signature, PCR Values
    deactivate D
```

## Step 1: Quote Request

The operator's machine requests a fresh quote by sending the encrypted AIK, a
nonce and the PCR indices that it wishes to be quoted (default: 0-8, 11-14).
The data send with the Quote Request is encoded as a protobuf message with the
fields in the table below.

| Field         | Contents                                                   |
|---------------|------------------------------------------------------------|
| `aik_public`  | `TPM2B_PUBLIC` structure of the Attestation Identity key.  |
| `aik_private` | `TPM2B_PRIVATE` structure of the Attestation Identity key. |
| `nonce`       | Random 32 byte nonce $\mathit{p}$.                         |
| `pcr`         | List of PCR indices to be quoted.                          |

The request is send as a HTTP GET to `/quotev0/request` with the content type
set to `application/protobuf; proto=quotev0.Request`.

## Step 2 & 3: Regenerate SRK and load AIK

The device will recreate the SRK and load the AIK sent with the
request. If the AIK has been manipulated in transit or the TPM has been
replaced loading the AIK will fail. The detailed parameters for the SRK are
defined in `enrollv0`.

## Step 4 & 5: Read PCR contents

The device reads the current value of all PCR requested in Step 1
from the TPM. This is necessary because the quote structure only includes a
hash of all PCR not their individual values.

The device only reads out the SHA-256 bank of each PCR.

## Step 6 & 7: Quote PCR contents

After reading PCR values and loading all needed keys the device requests a quote
over all PCR, signed with the AIK. The nonce sent by the operator's
machine will also be mixed into the signed quote as the *qualifyingData* field.
The PCR selection structure send to the TPM lists PCR indices in the order they
appear in the `pcr` field of the Quote Request message (Step 1).

## Step 8: Send Quote structure



The quote structure, a signature over it, a list of quoted PCR values, the UEFI
and stboot event logs are send back to the operator's machine. The data is
encoded as a protobuf message and send as response to the HTTP GET request
initiated in Step 1. The content type of the response is `application/protobuf;
proto=quotev0.Response`.

| Field        | Contents                                                        |
|--------------|-----------------------------------------------------------------|
| `quote`      | `TPM2B_ATTEST` structure containing the quote.                  |
| `signature`  | `TPMT_SIGNATURE` structure encoding the signature over `quote`. |
| `pcr`        | Dictionary of the PCR contents as quoted.                       |
| `uefi_log`   | Binary UEFI TPM 2.0 event log.                                  |
| `stboot_log` | Optional binary stboot TPM 2.0 event log.                       |

## Quote Verification

The final step is to verify the quote received in Step 8. The process requires
the operator to select the expected platform, stboot and OS package
measurements beforehand. They are stored in files described in the `endorsev0`
protocol. The verification routine takes the firmware measurements from the
platform endorsement file, replaces the hashes of the booted EFI application
with those from the stboot endorsement file and appends the measurements done by
stboot using the values from the OS package endorsement. This expected event
log is then replayed to compute the expected PCR values, which are compared to
the received ones. If they match they are hashed in the order of the `pcr`
field in Step 1 to produce the final, quoted hash. Then, the `TPMS_ATTEST`
structure is verified using the following steps.

1. The hash of all recovered PCR match the *pcrDigest* field,
2. the *magic* field is equal `0xff544347`,
3. the *type* field is equal `TPM_ST_ATTEST_QUOTE`,
4. the *qualifiedSigner* field is equal to the Qualified Name of the device's
   AIK under it's SRK, in the Owner Hierarchy, and
5. the *extraData* is equal to $\mathit{p}$.

Finally, the signature is verified. The signature is accepted if it is a valid
ECDSA signature by the AIK of the hash of the TPM2B_ATTEST structure.
