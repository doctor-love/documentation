---
title: Trust Policy
---

# Trust Policy

The trust policy for stboot contains a collection of settings in JSON format and the OS package signing root certificate files. 

## Source
These artifects must be located in the following locations inside stboot's initramfs: 
```
/etc/trust_policy/trust_policy.json
/etc/trust_policy/ospkg_signing_root.pem
```
## Trust Policy Settings

The trust policy settings are represented as a JSON object. For a normative
reference refer to the [JSON Schema defnitions](/trust_policy.schema.json). A
overview can be found in the table below. 

| Field                     | Type    | Valid Values           | Description                                                             |
|---------------------------|---------|------------------------|-------------------------------------------------------------------------|
| ospkg_signature_threshold | integer | >=1                    | Number of signatures that must be valid during OS package verification.
| ospkg_fetch_method        | string  | "network", "initramfs" | OS package boot mde

_Example_
```json
{
  "ospkg_signature_threshold": 2,
  "ospkg_fetch_method": "network"
}
```

# OS Package Signing Root Certificate

PEM encoded root certificate according to the OS package signature format.

_Example_
```
-----BEGIN CERTIFICATE-----
MIH/MIGyoAMCAQICEAgpUv/SsinmYwtBdb0jiUYwBQYDK2VwMAAwHhcNMjMwNjE0
MTcwNzQ3WhcNMjMwNjE3MTcwNzQ3WjAAMCowBQYDK2VwAyEAfQ/KmA6Wkabnu+Kx
Do60j2km2VPjXUYUCS+sYYcJ/1OjQjBAMA4GA1UdDwEB/wQEAwIChDAPBgNVHRMB
Af8EBTADAQH/MB0GA1UdDgQWBBTtkxYjBrEkaEJAdBrhZV4g/RhDejAFBgMrZXAD
QQCRA6riKLjwBQ9JPMddroUxjkWv0GEAIPwWnW4coalLRDlAVeUDVrG2a/WAlfbk
4kglOZSzeg1hRDZpct/0bGsI
-----END CERTIFICATE-----
```
