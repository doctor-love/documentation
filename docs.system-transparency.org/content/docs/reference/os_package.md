---
title: OS Package
---

# OS Package

An OS package is a set of two files, a descriptor and an archive. OS packages
are signed by one or more keys and contain a Linux kernel and initial RAM
file system together with a kernel command line. The descriptor contains the
signatures and certificates of the keys that generated those signatures. To
verify a OS package stboot will iterate though all signatures and verify that
the corresponding certificate was signed by the root signing key in the device's
trust policy, then that the signature is valid and signs the OS package
archive. If the minimum signature threshold configured in the trust policy is
reached the archive is unpacked the kernel and initial RAM disk is located and
started using the `kexec` system call. In order to locate files in the OS
package archive, each has a `manifest.json` file embedded. This manifest
contains paths to kernel, initial RAM disk, the kernel command line and a
descriptive label for the OS package.

OS packages are fetch either from disk or from the network. The location is
defined by the trust policy. In case that trust policy only defines the
descriptor location the descriptor contains the location of the OS package
archive. 

Both the OS package descriptor and the manifest are JSON files whose exact
shape is defined by JSON Schema definitions. The reference below was generated
from those JSON Schemas. 

The version fields of both documents are `1`.

## Schema Reference

This reference was generated from the authoritative JSON Schema definitions for
[OS package descriptors](/ospkg_descriptor.schema.json) and [OS package
manifests](/ospkg_manifest.schema.json).

The following tables give an overview of both documents.

**Descriptor**
| Field        | Type    | Description                                                           |
|--------------|---------|-----------------------------------------------------------------------|
| version      | integer | Version of the descriptor schema                                      |
| os_pkg_url   | string  | URL of the OS package zip archive. Must be a valid HTTP or HTTPS URL. |
| certificates | array   | List of certificates used to sign the OS package archive.             |
| signatures   | array   | List of signatures of the OS package archive.                         |

**Manifest**
| Field     | Type    | Description                                                |
|-----------|---------|------------------------------------------------------------|
| version   | integer | Version of the OS package manifest                         |
| label     | string  | Label of the OS package.                                   |
| kernel    | string  | Path to the kernel image inside the OS package archive     |
| initramfs | string  | Path to the initramfs image inside the OS package archive. |
| cmdline   | string  | Kernel command line the OS package will be booted with.    |
