---
title: stboot Measurements
---

# stboot Measurements

Before booting an OS package, stboot measures the keys used to verify it and
the provisioned device identity into the device's TPM. This document describes
these measurements in detail and the event log entries taken.

This is a summary of all measurements done by stboot itself before booting an
OS package. Each PCR's measurements are explained in more detail below.

| PCR | Measurement                               | Type         | Data                     |
|-----|-------------------------------------------|--------------|--------------------------|
| 12  | SHA256(OS package descriptor)             | `0xa0000000` | JSON descriptor          |
| 12  | SHA256(OS package archive)                | `0xa0000001` | Archive file name        |
| 13  | SHA256(Trust policy)                      | `0xa0000002` | JSON trust policy        |
| 13  | SHA256(DER signing root)                  | `0xa0000003` | DER certificate          |
| 13  | SHA256(certificate 1, certificate 2, ...) | `0xa0000004` | DER certificates         |
| 14  | SHA256(Human-readable ID)                 | `0xa0000005` | Human-readable ID string |

## PCR[12]: Detail Measurements

The detail measurements contain the OS package descriptor and archive as they
were received into PCR 12. The archive is measured first with the event type
`0xa0000000` and the file name as the event data. The measurement is the SHA-256 hash
of the zip archive that contains the OS package manifest, Kernel and initramfs.

After this the SHA-256 hash of the OS package descriptor is measured using
event type `0xa0000001` and the event data set to the JSON encoded descriptor.

Attesting the detail measurements allow users to verify a stbooted machine runs
a specific OS package but should always be combined with authority measures to
verify the measurements where done by a stboot bootloader.


## PCR[13]: Authority Measurements

The authority measurements cover the stboot configuration used to verify the
OS package but not the package itself, not the stboot code. The latter is
measured by the UEFI.

After the detail measurements stboot measures the

* SHA-256 hash of the trust policy JSON document,
* the SHA-256 hash of the DER encoded X.509 certificate certifying the OS
  package signing keys, and
* the SHA-256 hash of all DER encoded X.509 certificates used to verify TLS
  connections concatenated together.

The event type for the trust policy is `0xa0000002`, for the OS package certificate
`0xa0000003` and for the TLS certificates `0xa0000004`. All three measurements log the
contents of hash, i.e. the trust policy or DER certificate(s) are event data.

Users that don't care about the exact OS package but only want to verify
whether a device was booted using stboot can attest the authority measurements
without looking at the detail measurements.

## PCR[14]: Identity Measurements

The identity measurements are the final call and are recorded in PCR 14. There
is only a single measurement done which is the human-readable identity of the
device that was written into the TPM during provisioning. The measurement uses
event type `0xa0000005` and is the SHA-256 hash of the string provisioned by the
operator. The event data is this string itself.

Users only interested in the identity of the device and not it's state can
attest this PCR and the authority measurements to ensure the identity
measurements was done by stboot.
