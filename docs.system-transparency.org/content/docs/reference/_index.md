---
title: Reference
weight: 5
---

# Reference

This reference section is describing interfaces used in the System Transparency architecture. We describe them here and link explanations into the right section.

The picture below shows boxes with descriptions and arrows indicating producers and consumers of interfaces.
Boxes with coloured text are interfaces while boxes without colour are consumers (incoming arrow) or producers (outgoing arrow).

## Blue interfaces

File formats and data structures used as part of the ST.

## Green interfaces

Protocols used in communication with components.

## Red interfaces

Command-line or shell interfaces.

```mermaid
flowchart TB

%% TODO: come up with an alternative to using different colours to indicate interface type (document, protocol, cmdline)
%% See documentation at https://mermaid.js.org/syntax/flowchart.html for ideas.

    %% document formats
    hostconf([host configuration]); style hostconf color:blue
    trustpol([trust policy]); style trustpol color:blue
    ospkg([ospkg]); style ospkg color:blue
    endorsev0([endorsev0]); style endorsev0 color:blue
    measurementsv0([measurementsv0]); style measurementsv0 color:blue
    debug_something([debug_something]); style debug_something color:blue

    %% network protocols
    enrollv0([enroll0]); style enrollv0 color:green
    quotev0([quotev0]); style quotev0 color:green
    stprov_protocol([stprov protocol]); style stprov_protocol color:green

    %% command line interfaces
    stmgr_cmdline([stmgr command line]); style stmgr_cmdline color:red

    %% st tooling
    stauth_endorse_client{{stauth endorse client}}
    stauth_endorse_server{{stauth endorse server}}
    stauth_endorse_stboot{{stauth endorse stboot}}
    stauth_endorse_ospkg{{stauth endorse ospkg}}
    stauth_quote_client{{stauth quote client}}
    stauth_quote_host{{stauth quote host}}
    stboot{{stboot}}
    stmgr{{stmgr}}
    stprov_client{{stprov client}}
    stprov_server{{stprov server}}

    %% external parties
    build_scripts{{external build scripts}}

    stprov_server --> hostconf --> stboot
    stmgr --> trustpol --> stboot
    stmgr --> ospkg --> stboot

    %% rendering multi directional arrows (<-->) are buggy, but fixed in https://github.com/mermaid-js/mermaid/pull/4286
    stauth_endorse_server <--> enrollv0 <--> stauth_endorse_client
    stauth_endorse_client & stauth_endorse_stboot & stauth_endorse_ospkg --> endorsev0 --> stauth_quote_client

    stauth_quote_host --> quotev0 --> stauth_quote_client
    stboot --> measurementsv0 --> stauth_endorse_server
    stprov_client <--> stprov_protocol <--> stprov_server
    stboot --> debug_something --> stauth_quote_client
    stmgr --> stmgr_cmdline --> build_scripts
```
