---
title: OS Package Runtime Metadata
---

# OS Package Runtime Metadata

This document defines the mechanism and data format used by stboot to pass
metadata to the booted OS package. The data is generated during boot and may
differ between boots and platforms. This data is used during remote attestation
for debugging failed PCR pre calculations. The data is not necessary for remote
attestation to work under normal conditions and is purely informational. It
does not contain any confidential information.

## Format

The metadata occupies a continuous block in physical memory of at least 2 MiB.
It starts with the ASCII string `stboot metadata v1` followed by 14 zero bytes.
After this header, one or more metadata blocks follow. Each starts with a 4 byte
identifier followed by a 4 byte, little endian encoded length field. After that
comes the block specific data. The length field refers to the length of the
block data excluding the identifier and length field.

After a block another can follow. If the consumer sees a block identifier it
does not recognize it stops parsing. The following sections describe all know
block identifiers and their block specific data.

### stboot Event Log

The measurements done by stboot before handing execution off to the OS package
are collected in a TPM 2.0 event log. The block identifier is 0x474F4C45 and
the block specific data the binary event log. Concatenating this event log to
the UEFI event log produces a complete view of all measurements.

The log contains the stboot specific event log types described in the stboot
Measurements reference document.

### Human-readable Identity

The human-readable identity of the device that was provisioned into the TPM by
the operator. Block specific data is an arbitrary ASCII string. The string is
not NUL terminated. The block identifier is 0x44495855.

### Example

For illustration, below is an example metadata structure with two blocks, a
event log and a human-readable identity. The block list ends with an undefined
block identifier of all zeros.

| Offset (bytes) | Size (bytes) | Contents                                                                     |
|----------------|--------------|------------------------------------------------------------------------------|
| 0              | 32           | `stboot metadata v1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00` |
| 32             | 4            | 0x474F4C45 (Event log identifier)                                            |
| 36             | 4            | n                                                                            |
| 40             | n            | TPM 2.0 event log data.                                                      |
| 40 + n         | 4            | 0x44495855 (Human-readable Identity identifier)                              |
| 44 + n         | 4            | 16                                                                           |
| 48 + n         | 16           | srv1.example.com                                                             |
| 64 + n         | 4            | 0x00000000 (Unrecognized identifier)                                         |

## Transport

In order for the OS package Linux kernel to access the metadata without any
additional drivers stboot creates a fake persistent memory device and fills it
with the metadata. Despite the name the persistent memory device is not
persistent across reboots but is regenerated each boot. stboot uses the `memmap`
command line option for creating the device. After booting the device will appear
as `/dev/pmemX` and can be read like any other device or file. If there are
more than one `pmemX` nodes the stboot metadata can be identified by reading
the first 32 bytes and verifying it's `stboot metadat v1` followed by 14 zero
bytes.
