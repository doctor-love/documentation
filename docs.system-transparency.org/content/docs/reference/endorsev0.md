---
title: endorsev0
---

# endorsev0

`endorsev0` defines the on-disk format of the three endorsements
needed to verify a quote described in `quotev0`: platform, stboot and
ospkg endorsement.

The platform endorsement consists of the observed firmware, the exact
sequence of boot events and the Attestation Identity Key which signs
the quote. It is the result of an enrollment defined by `enrollv0`.

The stboot and OS package endorsements are generated directly from the
stboot ISO and OS package zip archive plus JSON manifest,
respectively.  The purpose is to store the expected hash values for a
remote attestation in an efficient way.

All three endorsement are encoded using protobuf messages. Each message is
wrapped in an envelope message to allow the consumer of an endorsement to
inspect it and for example detect a misuse (i.e. using a stboot endorsement
when a OS package endorsement is expected).

For detailed definitions of the TPM commands and structures used in this
document, refer to parts two and three of the [Trusted Platform Module Library
Specification](https://trustedcomputinggroup.org/resource/tpm-library-specification/)
published by the Trusted Computing Group.

## Platform Endorsement

The Platform Endorsement file includes all device specific firmware
measurements, the device's human-readable identity and the Attestation Identity
Key (AIK) that is expected to sign a quote. The file is the result of running
the `enrollv0` protocol with the device. A detailed description of all fields
in the Platform Endorsement is in the table below.

| Field         | Contents                                                                |
|---------------|-------------------------------------------------------------------------|
| `log`         | TPM 2.0 event log template.                                             |
| `aik_public`  | `TPM2B_PUBLIC` structure of the Attestation Identity key.               |
| `aik_private` | `TPM2B_PRIVATE` structure of the Attestation Identity key.              |
| `aik_qname`   | Qualified Name of the AIK, loaded under the SRK in the Owner Hierarchy. |
| `ux_identity` | Human-readable device identity string. Trailing zero bytes are trimmed. |

The `log` field is a templated TPM 2.0 event log [^1]. It mirrors the event log
received from the device but adds a type field to each element. This type is
used during the `quotev0` protocol to fill the right entries with the checksums
of the expected stboot and OS package artifacts. A list of all types defined in
`endorsev0` can be found in the table below.

| Type                 | PCR   | Description                                                                                    |
|----------------------|-------|------------------------------------------------------------------------------------------------|
| UNSPECIFIED          | N/A   | *Unused, invalid*                                                                              |
| INIT                 | *all* | Initial value of the PCR after boot. Defined by the TCG specification.                         |
| OPAQUE               | *all* | An uninterpreted firmware measurement. Used as-is.                                             |
| STBOOT_UKI           | 4     | Authentihash of the first phase stboot EFI application (shim stub).                            |
| STBOOT_LINUX         | 11    | Hash of the `.linux` section of the stboot UKI. Measured by the *systemd-stub*.                |
| STBOOT_INITRD        | 11    | Hash of the `.initrd` section of the stboot UKI. Measured by the *systemd-stub*.               |
| STBOOT_CMDLINE       | 11    | Hash of the `.cmdline` section of the stboot UKI. Measured by the *systemd-stub*.              |
| STBOOT_OSREL         | 11    | Hash of the `.osrel` section of the stboot UKI. Measured by the *systemd-stub*.                |
| STBOOT_AUTHENTIHASH  | 4     | Authentihash of the second phase stboot EFI application (LinuxBoot).                           |
| OSPKG_ZIP            | 12    | The OS package zip archive as downloaded.                                                      |
| OSPKG_DESCRIPTOR     | 12    | The OS package JSON manifest as downloaded.                                                    |
| OSPKG_SECURITYCONFIG | 13    | The trust policy as stored in the STConfig EFI variable or file.                               |
| OSPKG_SIGNINGROOT    | 13    | DER encoded certificate used to verify all OS package signers. Read from the trust policy.     |
| OSPKG_HTTPSROOTS     | 13    | All DER encoded certificates that verify TLS connections done by the stboot Linux environment. |
| UX_IDENTITY          | 14    | The human-readable identity of the device as read from the TPM.                                |

The *UNSPECIFIED* type is not used and no valid Platform Endorsement has an
entry of this type. The *INIT* type must always be the first entry in the log
for each PCR. It specifies the initial value of a PCR before any extension
takes place. The *OPAQUE* entries are firmware measurements done by the UEFI
and other components and are never replaced with new values. All other entry
types are replaced by values from the OS package and stboot endorsement files.

## stboot Endorsement

The stboot Endorsement file is generated from the bootable stboot ISO and
contains SHA-256 hashes of various artefacts. These hashes are used in the
`quotev0` protocol to compute the expected PCR values from a device.

A detailed list of the protobuf structure fields can be found in the table
below.

| Field             | Contents                                                                                       |
|-------------------|------------------------------------------------------------------------------------------------|
| `uki`             | Authentihash of the first phase stboot EFI application (shim stub).                            |
| `linux`           | SHA-256 hash of the `.linux` section of the stboot UKI. As measured by the *systemd-stub*.     |
| `initrd`          | SHA-256 hash of the `.initrd` section of the stboot UKI. As measured by the *systemd-stub*.    |
| `cmdline`         | SHA-256 hash of the `.cmdline` section of the stboot UKI. As measured by the *systemd-stub*.   |
| `osrel`           | SHA-256 hash of the `.osrel` section of the stboot UKI. As measured by the *systemd-stub*.     |
| `authentihash`    | Authentihash [^2] of the second phase stboot EFI application (LinuxBoot).                           |
| `security_config` | SHA-256 hash of the trust policy as stored in the STConfig EFI variable or file.               |
| `signing_root`    | DER encoded certificate used to verify all OS package signers. Read from the trust policy.     |
| `https_roots`     | All DER encoded certificates that verify TLS connections done by the stboot Linux environment. |

The *uki* field is the Microsoft Authenticode hash of the EFI boot application
`/EFI/BOOT/BOOTX64.EFI` on the VFAT filesystem embedded in the ISO image. The
*linux*, *initrd*, *cmdline*, and *osrel* fields are the SHA-256 hashes of the
respective PE section in the EFI application. The *authentihash* field is the
Microsoft Authenticode hash of the EFI application embedded in the `.linux`
section. The *security_config*, *signing_root*, and *https_roots* are SHA-256
hashes of files in the gzipped, CPIO formatted initial ram disk embedded in the
`.initrd` section of the boot application. These files are
`/etc/trust_policy/trust_policy.json`,
`/etc/trust_policy/ospkg_signing_root.pem`, and
`/etc/ssl/certs/isrgrootx1.pem` respectively.

## OS Package Endorsement

The OS package Endorsement file contains the SHA-256 hash of the OS package zip
archive and JSON manifest. Both are measured by stboot before calling kexec.
The hashes are used by the `quotev0` protocol to compute the expected PCR
values for a device.

The protobuf encoded message's fields are listed below.

| Field        | Contents                                      |
|--------------|-----------------------------------------------|
| `zip`        | SHA-256 hash of the OS package zip archive.   |
| `descriptor` | SHA-256 hash of the OS package JSON manifest. |

# References

[^1]: The event log format is described in section 10 of the TCG PC Client
      Platform Firmware Profile Specification
[^2]: The authentihash of a PE/COFF binary is described in the Windows
      Authenticode Portable Executable Signature Format specification.
