---
title: How to create a signed OS package
---

# How to create a signed OS package

This guide shows you how to create an OS package signed with your keys and ready to be fetched and booted by stboot.

In order to build up an OS package you need to provide a Linux kernel of your choice and an initramfs containing the usersapce for your service.
Now use the stmgr tool to package them:
```bash
stmgr ospkg create -label='System Transparency Example OS' \
-kernel=path/to/your-OS-kernel \
-initramfs=path/to/your-OS-initramfs \
-cmdline='console=tty0' 
```
Use the `-label` to add a short description of the boot configuration. With `-cmdline` you can define the command line for your OS kernel.

Now you have a zip-archived OS-package file and the corresponding descriptor file in your current working directory. Keep them together

The next step is to sign the package with your keys. See [here](./ospkg_keys.md) in case you don't have keys yet.
```bash
stmgr ospkg sign -ospkg path/to/your-os-pkg.zip \
-key=path/to/your-signing.key \
-cert=path/to/your-signing.cert 
```
Depending on your [Trust Policy](../reference/trust_policy.md) you may want to repeat this multiple times with different keys.

Run `stmgr ospkg -help` for a complete list of options.

At this point your OS package is ready to be placed at one of [supported locations](../explanation/ospkg_fetching.md) for stboot to fetch it.

