---
title: How to assemble the stboot image
---

# How to assemble the stboot image

This guide shows you how to create the stboot image for your host machine.
To do so it assumed that you have host config and trust policy files with your individual settings, signing root certificate, HTTPS root certificate and your linux kernel at hand.

TBD
- maybe link to precompiled kernel
- review u-root flags

```bash
u-root -build=bb \
-uinitcmd="stboot -loglevel=d" \
-defaultsh="" \
-uroot-source ./cache/go/src/github.com/u-root/u-root \
-o out/artifacts/initramfs:incl-hostconfig.cpio.tmp 
-files out/keys/example_keys/root.cert:etc/trust_policy/ospkg_signing_root.pem \
-files contrib/initramfs-includes/isrgrootx1.pem:etc/ssl/certs/isrgrootx1.pem \
-files out/artifacts/host_config.json:etc/host_configuration.json \
-files out/artifacts/trust_policy.json:etc/trust_policy/trust_policy.json \
github.com/u-root/u-root/cmds/core/init system-transparency.org/stboot
```

- describe UKI/ISO?

```bash
stmgr uki create \
-format iso \
-kernel=out/artifacts/stboot.vmlinuz \
-initramfs=out/artifacts/stboot.cpio.gz
```