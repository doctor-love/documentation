---
title: How to create OS package signing keys
---

# How to create OS package signing keys

This guide shows you how to produce X509 signing keys to sign OS packages with.

If you don't already have a root certificate and root key for your signing keys, create them first.
```bash
stmgr keygen certificate -isCA
```
Using the root key create one or more signing keys and corresponding certificates.
```bash
stmgr keygen certificate -rootCert=path/to/root.cert -rootKey=path/to/root.key
```

Run `stmgr keygen certificate -help` for a complete list of options.

Learn more about [OS package signature verification](../explanation/verified_boot.md).