---
title: How to setup the build environment
---

# How to setup the build environment

This guide shows you what tools and artifacts you need to get started with usage/development of
System Transparency. 

It assumes that your device is running Linux and has [Golang](https://go.dev/doc/install) version
1.17 or later properly installed.

1) Install _stmgr_ tool
```bash
go install system-transparency.org/stmgr@latest
````

2) Get _stboot_ source code
```bash
git clone https://git.glasklar.is/system-transparency/core/stboot.git
git -C stboot/ checkout <latest tag>
```
3) Get _u-root_ source code and install the _u-root_ initramfs builder
```bash
git clone https://git.glasklar.is/system-transparency/core/stboot.git
git -C u-root/ checkout v0.10.0
go install ./u-root
```
