---
title: System Management Mode
---

# System Management Mode

System Management Mode (SMM) represents the most privileged execution mode on
x86 processors. We will concer us with the Intel implementation only for now.
When operating in SMM, the code is afforded all the permissions typical of Ring
0, with the added benefit of accessing the TSEG memory segment. The memory
controller can be used to configure the address range that TSEG uses, through
the TSEG Memory Base register. However, it is common for this range to overlap
with the area traditionally used for memory mapping the VGA register file and
frame buffer, specifically within the lower 16 bits.

The Bootstrap processor initiates in SMM, which facilitates the UEFI's access
to TSEG to set up code and data used in SMM. Once SMM has been exited using
the RSM instruction, the only means to re-access it is through a System
Management Interrupt (SMI). Various peripheral events, such as attempts to
write the SPI flash, receiving a USB packet, or a configurable timer firing,
can trigger an SMI. In addition to peripheral triggers, software System
Management Interrupts provide a way to access SMM. These can be
initiated by writing into the I/O memory address 0x00b2.

Regardless of the trigger, all processor threads save their state - including
register contents - into a designated area in TSEG, then enter SMM and commence
the execution of the interrupt service routine for that specific SMI. These
service routines are established by the firmware in advance. Upon completion of
the routines, the system returns using the RSM instruction. During an SMI, all
counters on the host are halted in an attempt to shield SMIs from the host's
code.

## Uses

System Management Mode (SMM) is the oldest mechanism for firmware to
inject code into the system's runtime and maintain code execution even after
the operating system has been booted. Introduced with the Intel 486, SMM
predates both Advanced Configuration and Power Interface (ACPI) and Unified
Extensible Firmware Interface (UEFI) runtime services. Originally, SMM was
employed for mobile device power management, to implement network kill
switches, and to emulate PS/2 for USB keyboards and mice.

Given its concealment from the operating system, SMM permits platform vendors
to establish intermediary layers around proprietary operating systems that are
slower to adapt, such as Windows. In modern usage, SMM is harnessed for
implementing SPI write protection and controlling access for the onboard I2C
bus.

UEFI presents a modular SMM framework, which provides vendors with the
flexibility to install their own software SMI service routines. This ensures
seamless integration without interference with pre-existing ones.


## Security Impact and Mitigations

System Management Mode presents a potential risk to a Dynamic Root of Trust for
Measurement (DRTM) due to its inability to be measured or eliminated once
established by the UEFI. The code and data operating in SMM is part of the
firmware image and thus measured during boot. What complicates matters is that
specification permits SMM to be measured alongside the rest of the firmware in
one cumulative value. Consequently, any changes within SMM cannot be
distinguished from modifications to the rest of the UEFI firmware.

In Intel systems, two attempts have been made to enhance the visibility of SMM
by the Measured Launched Environment (MLE), i.e., the code deployed after DRTM.
The first is the System Management Interrupt Transfer Monitor (STM), and the
second is a duo of technologies named Intel System Resource Defense (ISRD) and
Intel System Security Report (ISSR).

The SMI Transfer Monitor introduces a secondary hypervisor that operates
entirely within SMM, managing virtual machines that also run exclusively within
SMM. Each System Management Interrupt (SMI) is first received by the hypervisor
and then forwarded to the appropriate virtual machine. This layering allows STM
to pervent the SMI handler routine from accessing all of the system. After DRTM
startup, the operating system can request the STM to deny specific resources to
SMI handlers. The STM will honor this request only if no SMI handler has
declared a need for that particular resource. The STM is measured as part of
the DRTM startup.

STM was eventually phased out in favor of ISRD & ISSR starting from the Tiger
Lake generation. Like STM, ISRD limits the privileges of the SMI handler code,
but instead of employing a second hypervisor, it utilizes regular paging. SMI
handlers declare their resource requirements, and ISRD enforces these by
running all handlers in Ring 3 and using fault handlers to selectively grant
resource access, such as Model Specific Register (MSR) and Input/Output (I/O)
memory. ISRD is implemented using Intel-provided binary code, avoiding reliance
on BIOS vendors to develop their own dispatcher as was the case with STM. Its
counterpart technology, ISSR, reuses STM functionality to allow a running MLE
to query the setup of ISRD protections. ISSR is also measured during the DRTM
launch.

## SMM and Remote Attestation

When doing only H-CRTM measurements the only way to attest the SMM code and
data is to track all host firmware measurements in PCR[0] that include,
directly or indirectly, the code and data set up in System Management Mode. In
the event log these measurements have the type 0x00000001 and the ASCII string
"SMM CODE" as event data, if the UEFI follows the latest TCG specification. How
the individual SMI handlers are set up, whether STM or ISRD is enables, and
what policies they enforce on the handlers is not part of the C-SRTM.

If the platform also uses DRTM and the operating system that is being attested
runs inside an Measured Launch Environment the SMI Transfer Monitor, but not
the SMI handler, are part of the DRTM measurements.

If instead ISRD is enabled ISSR will provide a signed statement over the
deployed SMM code, the handlers and their configured access rights.
