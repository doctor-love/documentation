---
title: Verified Boot
---

# Verified Boot

During the boot process stboot verifies the signatures of the received OS package before opening it. A bad signature verification will stop the boot process.

OS packages are signed by one or more entities by their corresponding certificates. These certificates must be signed by a common root certificate. The signatures and corresponding certificates are packed with the [OS package](./../reference/os_package.md).

The root certificate of the signing certificates and the minimum number of valid signatures signatures is compiled into stboot as part of the [Trust Policy](../reference/trust_policy.md).

The verification process in stboot is as follows:

- Validate the document format.
- For each signature-certificate- tuple:
  - Validate the certificate against the root certificate.
    - Only check validity bounds and if the certificate is chained to the trusted root certificate.
  - Check for duplicate X.509 certificates.
  - Verify signature.
  - Increase count of valid signatures.
- Check if the number of successful signatures is enough.