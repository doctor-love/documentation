---
title: Documentation
---

# Documentation

This website is built using [Hugo][] and the [Shadocs][] theme.

[Hugo]: https://gohugo.io/
[Shadocs]: https://shadocs.netlify.app/

## Building the web site locally

  1. Run `git submodule update --init --recursive`.
  2. Install Hugo
     - `apt install hugo` on a Debian system, or
     - follow the [installation instructions][https://gohugo.io/installation/] to install from source
  3. Serve and view the website locally
     1. Run `hugo serve`
     2. Browse http://localhost:1313

### Generating a new website for publishing

  1. Check that the website builds as expected locally (see above, step 3).
  2. Run `hugo` to produce the directory `public`, which can then be deployed to a web server.

## Structure of content

The content is structured with respect to both ST documentation
artefacts and the four types of documentation content defined by
[Diátaxis](https://diataxis.fr/).

* The documentation artefacts

  * High level intuitive description (overview, A.6.4.)
  * "System documentation" [1][] (sysdoc, A.6.5.)
  * Specification (spec, A.6.3.)
  * Design (design, A.6.2.)
  * Vision (vision, A.6.1.)
  * Documentation of each individual code artefact, outside of this repo (artefacts)

* The four documentation content types

  * Tutorials
  * How-to guides
  * Reference
  * Explanation

  Example: https://ubuntu.com/core/docs/

The left sidebar navigation might look something like this

  * Overview (type: explanation; from: overview)
  * Get started (type: tutorials, howtos; from: sysdoc)
  * Concepts (type: explanation; from: vision, design)
  * HowTo (type: howto; from: sysdoc?)
  * Troubleshooting (type: howto; from: sysdoc?)
  * Reference (type: reference; from: sysdoc?)

[1] Connecting the high level description to documentation belonging
to each ST code artefact involved. This seems like it will become the
kitchen sink.

## How to write documentation

The following guidelines are inspired by [Diátaxis](https://diataxis.fr/) and further sets some language styles to be consistent in ou documentation.

### How To Guides

For the essence of a How To Guide see [Diátaxis](https://diataxis.fr/how-to-guides.html)

- _This guide shows you how to…_

  Describe clearly the problem or task that the guide shows the user how to solve.

- _If you want x, do y. To achieve w, do z._

  Use conditional imperatives.

- _Refer to the x reference guide for a full list of options._

  Don’t pollute your practical how-to guide with every possible thing the user might do related to x.

- _~~You need to install x~~ Install x_

  Do not use personal subjects.

### Tutorials

For the essence of a Tutorial see [Diátaxis](https://diataxis.fr/tutorials.html)

- _In this tutorial, you will…_

  Describe what the learner will accomplish (note - not: “you will learn…”).

- _First, do x. Now, do y. Now that you have done y, do z._

  No room for ambiguity or doubt.

- _We must always do x before we do y because… (see Explanation for more details)._

  Provide minimal explanation of actions in the most basic language possible. Link to more detailed explanation.

- _The output should look something like this…_

  Give your learner clear expectations.

- _Notice that… Remember that…_

  Give your learner plenty of clues to help confirm they are on the right track and orient themselves.

- _You have built a secure, three-layer hylomorphic stasis engine…_

  Describe (and admire, in a mild way) what your learner has accomplished (note - not: “you have learned…”)

### Explanation  

For the essence of Explanation see [Diátaxis](https://diataxis.fr/explanation.html)

The reason for x is because historically, y…
Explain.

- _W is better than z, because…_

  Offer judgements and even opinions where appropriate..

- _An x in system y is analogous to a w in system z. However…_

  Provide context that helps the reader.

- _Some users prefer w (because z). This can be a good approach, but…_

  Weigh up alternatives.

- _An x interacts with a y as follows:…_

  Unfold the machinery’s internal secrets, to help understand why something does what it does.

### Reference

For the essence of Reference see [Diátaxis](https://diataxis.fr/reference.html)

- _X is an example of y. W needs to be initialised using z. This option does that._

  State facts about the machinery and its behaviour.

- _Sub-commands are: a, b, c, d, e, f._

  List commands, options, operations, features, flags, limitations, error messages, etc.

- _You must use a. You must not apply b unless c. Never d._

  Provide warnings where appropriate.

