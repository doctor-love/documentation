# Documentation

This repository contains documentation related to the [System Transparency project][].

[System Transparency project]: https://www.system-transparency.org/

## Structure

  - [archive/](./archive) - persisted pads and meeting minutes
  - [assets/](./assets) - project assets such as fonts, colors, and logos
  - [docs.system-transparency.org/](./docs.system-transparency.org) - source code for the project documentation website
  - [proposals/](./proposals) - proposals relating to design or the project at large

[The project website](https://www.system-transparency.org/) is built from [a separate repository](https://git.glasklar.is/system-transparency/project/system-transparency-landingpage).

## Contributing

Feedback to the development and processes of this project is most
welcome. Get in touch via the
[#system-transparency Matrix room](https://matrix.to/#/!system-transparency:matrix.org),
also linked to OFTC.net for those who prefer IRC.  Or through
[GitLab issues and merge requests](https://git.glasklar.is/system-transparency/).

Anything that requires a formal decision is decided on Wednesdays at 1200
UTC during [weekly project meets][].

[weekly project meets]: https://meet.sigsum.org/ST-weekly

## Maintainers

  - Linus Nordberg (ln5)
  - Rasmus Dahlberg (rgdd)

## Licence

CC BY-SA 4.0 unless specified otherwise.
