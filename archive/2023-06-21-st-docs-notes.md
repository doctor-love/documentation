# ST first impressions

## What is this?
Some random notes regarding initial impressions of System Transparency.

## Questions

### "What is ST?"
- From index page: "System Transparency is a security architecture for bare-metal servers."
- From index page: "System Transparency is a security architecture for bare-metal servers. stboot protects and makes your boot chain auditable. It uses LinuxBoot, TPM, and signature verification. It is reproducible, with plans to incorporate Binary Transparency. Correctly integrated it can make a system transparent to you and even your end users."
- From index page: "Link hardware and software: There are many ways to facilitate trust in the initial state of a system, with varying trade-offs and levels of assurance."
- Any newer talks? Last is from 2020


### "Who is it for?"
- Organisations operating bare-metal server infrastructure

### "Which problems/challenges does it solve right now?" (post shim signing)
- "Two-man rule" for software deployments (requires multiple operators/devs to sign OS package)
- Cryptographically verified network network booting (define how this is different from UEFI HTTPS boot)
- Provides somewhat immutable/non-persistent systems (reset upon reboot)

### "Which problems does it plan to solve?"
- Verified, meassured and attested boot on commodity HW

### "Which problems does it not solve?" (threat model)
- No protection against malicious HW
- Post-boot intrusion/compromise? (compared to Keylime, which provides continous execution monitoring using IMA)

### "What is missing/known problems?"
- ....

### "What are the different compontents?"
AKA "what roles do the different compontents play/what problems do they solve?"

#### Keykeeper
- .....

#### sthsm
- Tool for managing sign keys stored in a HSM - currently primarily used as an internal tool to sign UKIs for stboot releases

#### sthsm-tools
- .....

#### stshim
- Provides verified and meassured launch of stboot on systems with UEFI Secure Boot enabled (with "default vendor pre-loaded" allow DB/CA trust store)

#### stboot
- Minimal OS/boot environment to load/verify OS packages from network/disk and start them


## Random notes
- What does the demos show? A short description might be nice
- Don't just demonstrate/document "happy path", show what happens when things go wrong/when ST protects as intended
- Which are the deployment options (Coreboot + Linuxboot, UEFI shim, etc)? What are their pros/cons?
- Hardware options/requirements? (arc, TPM, etc)
- Any value for virtualised environments? What if cloud/infra provider is running st and exposing/forwarding TPM to VMs?
- What role does reproducible builds play? Any special reason for ST?
- What about a Dockerfile for build environment?
- "Make sure you have at least go1.17 running on your system. See here for installation.", running with latest (1.20.5)
- https://git.glasklar.is/system-transparency/project/st-shim-review/-/blob/st/ST_BOOT_PROCESS.md : Quite good overview!
- Would be nice with a glossary/definition of terms like "Platform operator"
- I guess that many end-users will want to use shim+stboot built and signed by the project and build their own OS package. Think documentation/READMEs should note what is for "ST developers" and "end-users"
- Trust policy and signing CA certificate for OS packages are baked in to initramfs which is a part of the UKI. How does that work with the projects signed shim?
- Usecase for "Initramfs Boot"? Isn't initramfs baked in to (signed) stboot UKI?
- Trust policy == How many trusted signatures are required to launch OS package + how may the OS packaged be fetched (initramfs or network) (?)
- 's/LinuxBoot/stboot/' ?
- Attestation as authentication for fetching OS package? What would be the value? Why do we want identity/authentication values as they are now? How does it align with the transperancy vission
- Is example OS really reproducible? Packages are fetched without specifying version?
- May be useful to write documentation in "book form" to explain motivations, considerations, why these complex things are done, etc

- Trying demo #1
	- Assets are still downloaded from Github: "https://github.com/system-transparency/example-os/releases/download/v0.1/ubuntu-focal-amd64.vmlinuz", instead of https://git.glasklar.is/system-transparency/core/example-os
	- "[initramfs:build] 20:55:09 WARNING: You are not using one of the recommended Go versions (have = go1.20.5, recommended = [go1.17])."
	- Document requirements for qemu-setup: task: command not found: "qemu-system-x86_64"
	- Required packages to get demo running on Ubuntu 22.04 LTS: sudo apt install --no-install-recommends qemu-system-x86 ovmf
	- What did we just demo?
	
- Trying demo #2
	- Fails after running demo #1 without prior cleanup: "[iso-provision] 21:09:34 ERROR: /home/qx/system-transparency/cache/go/pkg/mod/system-transparency.org/stmgr@v0.2.1/stmgr.go:20: failed to make iso: could not create device out/stboot.iso: file exists", really all tasks - a bit annoying
	- "The next time stboot starts with this EFI NVRAM it will find proper host configuration and use that information to configure the network and find an OS package to fetch, verify and boot.": How do we test this? What task options should be included?
	- Stuck printing "EFI stub: Loaded initrd from LINUX_EFI_INITRD_MEDIA_GUID device path" over and over again to serial console. Perhaps an error message could be good here?
	- "To get back to provisioning, simply remove the file backing the EFI NVRAM and boot the ISO again." Perhaps better to recommend the "clean-efivars" task?
	- "stprov remote static -h myhost -i 10.0.2.10/27 -g 10.0.2.2 -A": Won't work unless changed to /24
	
	
	

