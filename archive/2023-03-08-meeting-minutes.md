# System Transparency weekly

  - Date:  2023-03-08 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230308
  - Chair:

## Meeting canceled

Philipp and Linus were present but decided not to run this meeting.

