# System Transparency weekly

  - Date:  2023-04-12 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230412
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Jens
  - Foxboron
  - Philipp
  - Kai

## Status rounds

  - ln5: Thought about and discussed with some of you the pros and
    cons of monorepos, but really more about what users and usage we
    expect and how a single version for all the artifacts would affect
    both the development process and the users
  - Foxboron: sbat support done
  - Philipp: stpki for HSM provisioning
  - Kai: Thoughts on monorepo and versioning; exploritative coding on
    stauth; relocated to asia; will do FOSS-ASIA thu+fri
  - Jens: Road map thinking

## Decisions

  - None.

## Next steps

  - Philipp:
    - stpki tooling for the yubiHSM provisioning and certificate generation
    - shim building
    - code signing certificate requirements
    - review mortens code
  - Foxboron
    - mkuki integration into tasks
  - Kai:
    - Finalize the stauth protocol and roles. At least have a writeup
      ready to be presented (https://git.glasklar.is/kai/stauth)
  - Jens
	 - Finish planning with ln5
	 - Given the outcome of planning, either reorganise or just keep going

## Other

  - Next week is Glasklar meet-up in Stockholm tue-thu, so no ST weekly next wednesday, April 19
  - ln5 will be conferencing and vacating for 2,5 weeks starting April 24, back on May 10
  - Input to the "reorganisation discussion" by next tue (april 18) lunch would be valuable
    - stauth will have two important interfaces
      - the quote, being the hardest to change in the future
      - communicating between provisioning phase and "the future"
