# System Transparency weekly

  - Date:  2023-07-05 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230705
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - kfreds
  - Foxboron

## Status rounds

  - Philipp
	  - Working with Linus on sthsm and shim CI + testing
  - Kai
	  - Figuring out what is going wrong with EFI binary signing and sthsm-tools + go-uefi from morten
  - ln5
    - HSM provisioning
    - sthsm
    - reviewing a few MR's from Kai
  - Foxboron
    - Looking at stauth

## Decisions

  - Cancel next week's meeting (2023-07-12)

## Next steps

  - Philipp
	  - See above
  - Kai
	  - See above
  - ln5
    - HSM provisioning
    - Conference (PETS)
  - kfreds
    - Work planning with Linus for the period until the next Glasklar meetup in August
    - Feedback on stauth for Kai

## Other

### ST demo's not working any longer
  - Boot stopping after the following line, maybe UEFI is stopping here?
    EFI stub: Loaded initrd from LINUX_EFI_INITRD_MEDIA_GUID device path
  - Increasing the amount of RAM given to qemu doesn't help this
  - The CI/CD pipeline has seen this too, and so has Kai (we think)

### Current signing shim problem

 - It's hard to guess what the issue is
 - 1000's of people are signing binaries using go-uefi without issues
 - SBAT section, checksum, SBverify, obscure bug, that should have been released and pushed out
 - Is it the binary generation? Are we doing something wrong
 - Are Philipp and Kai testing with a very old sbverify?
   - no it's the newest
 - Is it only because sbverify doesn't like the signature?
   - it has to do with the pkcs7 container
 - Has Philipp found a test machine that doesn't boot?
   - no not really

Two softwares:
 - go-uefi
   - We're using it for it's knowledge of PE binaries?
 - sbverify (the upstream is kernel.org?)

#### Question
What are we using go-uefi for?
  - signing the efi binary like stboot.efi
What are we handing it? (inputs)
  - efi binary and certificate to use in hsm
What are we getting from it? (outputs)
  - signed efi binary

Are we using go-uefi for something related to the creation of the shim that we
re sending to the shim review committee?
  - no it is used to sign the binary loaded by the shim. The certificate tho to sign the the shim comes from the hsm

#### What has to be true for this issue to be a blocker for sending in the shim?

Scenario A:
 - go-uefi is behaving correctly
 - sbverify is behaving correctly
 - The tool that outputs the shim artifact is NOT behaving correctly
   - E.g. a section misalignment?

In other words this bug could be indicative of an issue with the shim artifact, which could make boot fail on some platforms? - kfreds
