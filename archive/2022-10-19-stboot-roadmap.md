# Plan forward

The code in stboot, stmgr and system-transparency(tooling) repos will tagged/versioned with v0.1.0 to keep the current working state.
The upcoming refactoring does not make any commitments in terms of backwards compatibility.

# Features we won't support now but expect to bring back at some point

- stboot as a coreboot payload
- localboot

# Features we'll remove and don't expect we'll bring back

- Intel TXT support
- In host configuration: Timestamp
- For other things than the OS package:
  - Mounting disk partitions (boot and data)
  - Storing anything on disk
- Remove the skip-tls-verify option from the kernel cmdline in stboot

# Bootchain

## stboot can be started by

- UEFI
  - stboot on a virtual USB stick (served by the BMC)
  - stboot on a real USB stick or disk

## OS package can be started by

- stboot

## Roadmap

stboot can be started by

- coreboot
- Firecracker
  - stboot as the thing that Firecracker boots in the guest 
    - Kernel: Uncompressed ELF
    - root fs: initramfs cpio OR file with a file system that the kernel supports
    - kernel cmdline

# What does stboot and OS packages expect from the environment?

## stboot needs

- Trust policy (baked into initramfs at /etc/trust_policy/)
  - trust_policy.json (renamed from security_configuration.json) 
    - Version
    - Valid signatures threshold
    - Fetching mechanism (Boot mode) 
      - (which in turn decides whether to use the network or not)
  - Other things here (as files) 
    - ospkg_signing_root.pem
  - In the future 
    - Trusted Sigsum logs
    - Minimum number of Sigsum witnesses required
    - Trusted Sigsum witnesses
- Host configuration storage
  - Version
  - Id/auth
  - Network configuration 
  - Network Mode (static IP, DHCP)
  - Host IP
  - Gateway
  - DNS Server
  - Network interface / MAC address
  - Bonding settings
  - HTTPS root certificate
  - Fetch mechanism configuration 
    - Which in turn must contain a pointer to the OS package descriptor
    - The output should always be a Descriptor and an OS package
- Descriptor:
  - Version
  - Source of OS package zip file
  - OS package signature
  - Corresponding certificates
- OS package zip archive includes at its top level exactly 3 files explicitly named:
  - "kernel"
  - "initramfs.cpio"
  - "cmdline.txt"
- Optional: TPM

In the case of our netboot method, host config contains:
- OS package descriptor URL

## New fetch mechanism

Fetch the OS package descriptor and the OS package from the initramfs of stboot

## OS package needs

- Host configuration storage
  - id/auth for id/auth
  - network configuration
- Optional: TPM

# Main Stages of stboot

Make sure it's easy to understand "the main() function" in stboot. What I mean by this is that main() should make it clear that stboot does the "main stages of stboot" above. The technical details of doing each step should be abstracted away in functions such as verify(), measure(), execute(), or similar. Those functions in turn should themselves be clear. Should we set a LOC max limit per function? Also, what about side effects? Document in the code explicitly what has side effects?

## Fetch host configuration (auto-detect where)

- order: initramfs, (coreboot fs), EFI NVRAM, (Firecracker)

## Fetch OS package descriptor

- Our current expectation: it's a fixed format regardless of fetch mechanism for descriptor as well as OS package.
- Future discussion: url flexibility or not in for the netboot method? e.g. https://descriptor with local://os_pkg.zip
- How do I fetch it? 
  - Which code path is executed depends on the method, and the method might require additional field in the host configuration.
- Is identity / auth used for other things? 
  - Yes. The OS package will use it.

## Verify OS package

- Trust policy

## Measure OS package if TPM is present

# Execute OS package

- Via kexec file load
  - Contents: kernel, initramfs, kernel cmdline

# kernel cmdline options

- debug
- dry-run

# TPM strategy

- TPM quote / seal is only used for essentials
- Quote: Remote attest to operator and VPN app
- Seal: Seal/unseal secrets stored in NVRAM

(Note: No new TPM work until December)


# Goals before December meetup

Everything runs in GitLab with CI and integration tests

Project management Kanban-style in GitLab with issues

Get rid of all the code we don't need anymore

Implemented the new fetch mechanism

Network boot up and running with the new code structure

Improved and tested the network code

## Stretch goals

Having the OS package code unit tested

Have a complete review of the current documentation and see how it looks on the godoc server

Increase test coverage and improve documentation

Make sure that stboot and the producer utility uses the same library code

library code: The packages Jens writes in the stboot repository

E.g. stboot consumes the host config and the OS package

(regarding a rewrite of the producer utility into a command line utility: leave out of scope before December)

# Plan from our meetup

1. Release a new version of mpt and deliver to Mullvad

2. Glasklar runs Sigsum v1 etc

3. "Restructure things"

- mpt integrate with stboot?
- What did we mean? They should use the same libraries when relevant.
- Warning! If we DO make mpt into a OS package it's possible that we might want to change mpt's architecture completely and e.g. not use u-root. Leave a large rework of mpt for now and let's discuss it in December.
- split libs? 
  - See plans above. In scope.
- what should tooling look like? 
  - See plans above. Out of scope.
- code restructuring and planning 
  - See plans above. In scope.
- better error handling 
  - See plans above. In scope.
- finish refactoring 
  - See plans above. In scope.
- feature questions 
  - See plans above. In scope.
- test coverage and integration tests 
  - See plans above. In scope.

4. Bring up more known hardware with ST

5. Measured boot using TPM

6. OS package with Firecracker VMM running VMs that have been measured

