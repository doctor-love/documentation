# Sem Transparency weekly

  - Date:  2023-08-16 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230816
  - Chair: jens

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - Jens
  - Linus
  - Kai
  - dotor-love

## Status rounds

  - Linus:
    - still vacating until next week, just listening
  - Kai:
    - random stuff on stboot regarding measurement
    - further thinking on stboot running on secure-boot enabled machines
    - documentation
  - Jens:
    - no updates this week

## Decisions

  - <add decision here>

## Next steps

  - Kai: continue, see above
  - Jens: get back to stboot MRs and Milestones

## Other

  - discussion: what is the right way to bring st to users (task, scripts, different users...)
  - make this a topic for the august meeting
