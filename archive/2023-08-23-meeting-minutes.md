# System Transparency weekly

  - Date:  2023-08-23 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230823
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Kai
  - Jens

## Status rounds

  - Kai: working on getting the stboot TPM design doc ready for the meetup next week. Sneak preview: https://git.glasklar.is/system-transparency/core/stauth/-/blob/mistress/tpm-backed.md
  - ln5: back from vacation; catching up; discussed ST releases with jens
  - Jens: review, PM and planning

## Decisions

  - Cancel the weekly of next week due to meetup. Next meeting will be on the 6th of sept.

## Next steps

  - Kai: Prepare for next week, take a look at MR 107
  - ln5: finish roadmap update draft work in time for meetup; attend meetup
  - Jens: roadmap draft, meetup prep, push documentation work to MR's

## Other

  - Roadmap update discussion
    - background: 
        - how about synching roadmap from may to gitlab milestones?
        - but hmm, we need to review the roadmap first
    - a recurring question is when we are allowed to break compatibility -- what are the expectations on ST *interfaces* stability?
    - idea: assume we don't care about users, for the moment, what would be best from a development perspective?
    - AP: make *two* roadmap drafts, representing the two different user perspectives 
        - one with each artifact version separately defined, to benefit for the "pick & choose" user, for each ST release
        - one without promises of even keeping the artifacts around, more like "ST release X contains the following features", giving no guarantees about any part of ST release X working with ST release X+1
    - AP: name all interfaces, document them and give them version numbers
      - interfaces being:
          - document formats (host config, trust policy, ospkg format)
          - network protocols (stauth endorse, enrole and quote)
          - name-needed (host config and trust policy search paths, ospkg fetching methods)

