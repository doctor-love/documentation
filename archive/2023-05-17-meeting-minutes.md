# System Transparency weekly

  - Date:  2023-05-17 1200-1250 UTC (14:00-14:50 CEST)
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230517
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Fredrik
  - Kai
  - Philipp
  - Jens

## Status rounds -- what has happened since last meeting

  - ln5
    - sthsm/stpki input to Philipp
    - Top level milestones in GL https://git.glasklar.is/groups/system-transparency/-/milestones
    - Bought a Chromebook and (almost) liberated it (Gentoo)
    - Tried to organise R.1. (network booted stboot) and R.2. (provisioning poc)
  - Philipp
    - Been coding on stpki and testing it
    - Signing the EFI file
  - Jens
    - stprov support in stboot
    - state of the tooling

## Decisions

  - Adopting [2023-05-12 road map](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-05-12-roadmap.md)

## Next steps

  - ln5
    - Get R.1. out the door by Friday
    - Help getting stprov (and dependencies) in a state where it can be demoed on Tuesday afternoon
    - Go to Gothenburg and hack on ST things for three days
  - kfreds
    - Formulating questions for Kai https://pad.sigsum.org/p/ST-QnA-lowlevel
    - Formulating TKey apps related to ST for next week
  - Kai
    - Writing documentation and researching
    - Attending the Tillitis meetup in Gothenburg
  - Philipp
    - Coding towards the deadline
    - Testing signing and verification of the shim in qemu
  - Jens
    - R.1. plus stprov support

## Other

  - We're all going to be in Gothenburg next week
    - Let's think about things to do that require high bandwidth, and schedule those things
    - Let's cancel the next weekly, on the 24th, since 1) the need for synchronising is probably low then, and 2) some of us might be traveling
  - How should we demo things next week?
    (Forking this out to separate meetings)
    - AP ln5: Prepare an agenda in a pad, markdown, which we use for presenting the whole thing, opening links in new tabs in a browser, simple and easy
    - Netbooted stboot (ln5, Philipp)
      - 1st option: Qemu using HTTP and DHCP server on KVM host (laptop) -- the OVMF package may be able to netboot (HTTP and PXE?)
        - AP Philipp: Investigate
      - 2nd option: Qemu might even provide the HTTP and DHCP server
        - AP Philipp: Investigate
      - 3rd option: Configure our own key in the "BIOS setup" of any Supermicro we can find
        - Let's not prepare for this
    - stprov
      - TBD
   - The HSM setup we're using for EFI shim signing
     - AP Philipp: Prepare this
