#!/bin/bash

#
# Similar functionality as `task iso-provision qemu:iso`, but as a bash script.
#

set -eu

ST=$(realpath $(dirname $0)/st)
SRC=$(realpath $(dirname $0)/src)
GOPATH=$(realpath $(dirname $0)/go)
CONTRIB=$(realpath $(dirname $0)/contrib)

function info() {
	echo "******" >&2
	echo "* INFO: $@" >&2
	echo "******" >&2
}

info "if things fail you might need to apt-install a bit"
info "fixing Go dependencies"

mkdir -p $SRC
[[ -d "$SRC/u-root" ]] || git clone --depth 1 --branch v0.10.0 https://github.com/u-root/u-root $SRC/u-root
[[ -d "$SRC/stboot" ]] || git clone --depth 1 --branch v0.2.0  https://git.glasklar.is/system-transparency/core/stboot.git $SRC/stboot

go install system-transparency.org/stprov/cmd/stprov@v0.1.1
go install system-transparency.org/stmgr@v0.2.1
go install github.com/u-root/u-root@v0.10.0

info "enusuring we have a kernel"

mkdir -p $CONTRIB
url="https://git.glasklar.is/system-transparency/core/system-transparency/-/raw/main/contrib/linuxboot.vmlinuz?inline=false"
[[ -f $CONTRIB/kernel.vmlinuz ]] || curl -L $url -o $CONTRIB/kernel.vmlinuz

info "seting up a basic PKI and trust policy"

mkdir -p $ST
stmgr keygen certificate -isCA -keyOut=$ST/root.key -certOut=$ST/root.crt
stmgr keygen certificate -rootKey=$ST/root.key -rootCert=$ST/root.crt -keyOut=$ST/leaf.key -certOut=$ST/leaf.crt

echo "INFO: Creating trust policy" 2>&1
cat << 'EOF' > $ST/trust_policy.json
{
  "ospkg_signature_threshold": 1,
  "ospkg_fetch_method": "initramfs"
}
EOF
stmgr trustpolicy check "$(cat $ST/trust_policy.json)"

info "building initramfs that contains stprov"

u-root -uroot-source=$SRC/u-root -o $ST/stprov.cpio -files $GOPATH/bin/stprov:bin/stprov all
gzip -kf $ST/stprov.cpio

info "creating a signed stprov OS package"

stmgr ospkg create -out=$ST/stprov-ospkg.zip -kernel=$CONTRIB/kernel.vmlinuz -initramfs=$ST/stprov.cpio -cmdline="console=ttyS0"
stmgr ospkg sign -key=$ST/leaf.key -cert=$ST/leaf.crt -ospkg $ST/stprov-ospkg.zip

info "building stboot ISO with stprov OS package"

url=https://git.glasklar.is/system-transparency/core/system-transparency/-/raw/main/contrib/initramfs-includes/isrgrootx1.pem?inline=false
[[ -f $CONTRIB/le.pem ]] || curl -L $url -o $CONTRIB/le.pem

u-root\
  -uroot-source=$SRC/u-root\
  -uinitcmd="stboot -loglevel=d"\
  -defaultsh=""\
  -o=$ST/stboot.cpio\
  -files=$CONTRIB/le.pem:etc/ssl/certs/isrgrootx1.pem\
  -files=$ST/root.crt:etc/trust_policy/ospkg_signing_root.pem\
  -files=$ST/trust_policy.json:etc/trust_policy/trust_policy.json\
  -files=$ST/stprov-ospkg.json:ospkg/provision.json\
  -files=$ST/stprov-ospkg.zip:ospkg/provision.zip\
  core $SRC/stboot/
gzip -kf $ST/stboot.cpio

rm -f $ST/stboot.iso
stmgr uki create -cmdline="console=ttyS0" -format=iso -out=$ST/stboot.iso -kernel=$CONTRIB/kernel.vmlinuz -initramfs=$ST/stboot.cpio.gz

ovmf_code=""
for str in "OVMF" "edk2/ovmf" "edk2-ovmf/x64"; do
  file=/usr/share/$str/OVMF_CODE.fd
  if [[ -f $file ]]; then
    ovmf_code=$file
    cp /usr/share/$str/OVMF_VARS.fd $ST/OVMF_VARS.fd
  fi
done

if [[ -z "$ovmf_code" ]]; then
  echo "ERROR: failed to locate OVMF_CODE.fd" 2>&1
  exit 1
fi

info "trying to run with qemu"

qemu-system-x86_64 -nographic\
  -m 512M -M q35 -rtc base=localtime\
  -net user,hostfwd=tcp::2009-:2009 -net nic\
  -object rng-random,filename=/dev/urandom,id=rng0\
  -device virtio-rng-pci,rng=rng0\
  -drive if=pflash,format=raw,readonly=on,file=$ovmf_code\
  -drive if=pflash,format=raw,file=$ST/OVMF_VARS.fd\
  -drive file=$ST/stboot.iso,format=raw,if=none,media=cdrom,id=drive-cd1,readonly=on \
  -device ahci,id=ahci0\
  -device ide-cd,bus=ahci0.0,drive=drive-cd1,id=cd1,bootindex=1
