# System Transparency weekly

  - Date:  2023-03-22 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://pad.sigsum.org/p/ST-20230322
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - zaolin
  - kfreds
  - foxboron

## Status rounds

  - ln5: Helped out with an MPT ISO not initialising a certain 10G NIC
  - ln5: Started debugging the stboot download issues
  - ln5: Planning for stprov
  - ln5: Returned the YubiHSM's
  - zaolin: Busy with other things, finally returning to Glasklar work
  - foxboron: stmgr mkuki stuff merged
  - jens: prepared integrating initramfs boot for stboot (quick and dirty)

## Decisions

  - None.

## Next steps

  - ln5: Ask Rasmus if he can be our secondary security operations contact person for the UEFI SHIM review application
  - zaolin: Returning to shim review and documentation
  - foxboron: mkuki integration into task
  - jens: adapting toooling to be able to integrate os package into initramfs

## Other

  - How badly do we need a server with a SuperMicro X10 board?
	  - Let's skip it, they're similar enough to X11. We will provide documentation.
  - May meeting location would be good to have confirmed soon.
    - Will do no later than upcoming Monday.
  - Kai probably available, part time, from April 3.
  - Credential binding might be a reasonable next step for Philipp, but let's see.
  - Shim review, documentation : https://git.glasklar.is/system-transparency/project/st-shim-review/-/blob/st/ST_BOOT_PROCESS.md
