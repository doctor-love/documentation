# ST road map

This document lives at https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/archive/2023-04-13-draft-roadmap-draft.md

`*** DRAFT DRAFT DRAFT DRAFT DRAFT ***`

## Artefacts, ie things we release, publish and maintain

- A.1. stlib
  - stlib (golang library for All The Things ST)
  - Tooling for building an os-pkg
  - Tooling for signing an os-pkg
- A.2. stboot
- A.3. stprov
- A.4. stauth (implementing STAM, server and client)
- A.5. stvmm
- A.6. Documentation
  - A.6.1. ST vision (kfreds)
  - A.6.2. ST design
    - Including a threat model, and clearly stating what is in scope
      and out of scope for ST in each of its supported configurations
      (based on the yet to be realised configuration-and-properties
      matrix)
  - A.6.3. ST specification (ln5)
    - Defining interfaces, in the broader sense. Including stboot
      trust policy and host configuration formats, the os-pkg format,
      the stauth quote format, and more.
  - A.6.4. High level intuitive description *with pictures* (ln5)
  - A.6.5. System documentation
    - Bridging the gap between the bigger picture from A.6.4 and
      documentation for each artefact

## Releases and what new functionality they provide

Releases are listed in *roughly* chronological order.

- R.1. Basic documentation
  - project/documentation vR.1.
    - ST specification (A.6.3.)
    - High level documentation (A.6.4.)
    - System documentation (A.6.5.)

- R.2. stboot early
  - stboot v1.0.0-rc.1
    - stboot is UEFI-booted from an ISO
    - Support for reading configuration from either EFI variables or initramfs
    - New general os-pkg fetch mechanism, supporting HTTPS, local storage and initramfs
    - Using Sigsum signatures for verifying os-pkg's
  - project/documentation vR.2.

  Rationale for R.2.: To get feedback on stboot before v1.0.0, so to
  avoid having interface changes and thus have to go to v2.0.0 when we
  find bugs or missing features.

- R.3. The vision
  - project/documentation vR.3.
    - The vision (A.6.1.)

- R.4. stboot reborn
  - stboot v1.0.0
  - project/documentation vR.4.

- R.5. Network booted stboot
  - stboot v1.1.0
    - Tooling and documentation for network booting stboot as a UEFI application
      - Support for being network booted over HTTP(S)
      - Documentation on how to do PXE booting
    - TODO jens: Check whether there are requirements from an stboot
      perspective that will result in interface changes and thus need
      a major revision bump (to v2.0.0)
  - project/st-shim-review v1.0.0
    - A Glasklar UEFI shim, with documentation of the process of getting a shim signed
  - project/documentation vR.5.

- R.6. Provisioning PoC
  - stprov v0.1.0
    - Proof of concept provisioning tool
  - TODO ln5: define the go/nogo-to-provisioning mechanism

- R.7. Verification PoC
  - <artefact TBD> <version TBD>
    - TBD some form of minimal remote attestation TBD

- R.8. Something to boot, take one
  - stlib v1.0.0
    - Building reproducible os-pkgs based on Debian
    - Go library packages moved here from stboot
  - project/documentation vR.8.

- R.9. Provisioning
  - stprov v1.0.0
    - A drop in replacement for MPT
  - project/documentation vR.9.

- R.10. coreboot -> stboot
  - stlib vTBD
    - stboot booted by coreboot
  - stboot vTBD
    - stboot reading its configuration from CBFS
  - project/documentation vR.10.

- R.11. Virtualization PoC
  - stvmm v0.1.0
    - Proof of concept ST VM manager
    - Technology used: TBD

- R.12. Verification
  - <artefact TBD> <version TBD>
    - TBD what to include
    - TBD including credential binding?
  - project/documentation vR.12.


### Release dates

TBD


`*** DRAFT DRAFT DRAFT DRAFT DRAFT ***`
