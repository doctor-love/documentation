# System Transparency weekly

  - Date:  2023-10-02 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: philipp
  - Secretary: rgdd

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - rgdd
  - kai
  - philipp
  - Nisse
  - Linus
  - Daniel
  - Jens

## Status rounds

  - rgdd: tinkering with stprov tests+ci, reading, feedback, filing issues, ...
    - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/9
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-09-29-stboot-with-stprov-ospkg.sh
    - https://git.glasklar.is/system-transparency/core/system-transparency/-/issues/220
  - nisse: tinkering with qemu-booting, minimal kernel + custom init process.
  - nisse: First license/authors MR in review: https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/113
  - nisse: Revised gitlab proposal, below.
  - kai: wrote explainers for TPM and shim. Got all the reference docs merged. Researched AMD SEV.  Going through feedback from nisse.
  - philipp: Released new theme for docs. Figured out issues with the current theme and tried to find a new theme which fits our requirements. Updated Secure Boot docs based on feedback from quite. Closed-down the System Transparency Slack.
  - quite: review secure boot, shim etc proposals. realized that stprov demo didn't work, and began digging. begun learning a bit more about servers, BMC, those OOB interfaces.
  - Linus: cleared-up github situation, now it's just a mirror.  If you think there's something that should be mirrored there that isn't, let me know.  Behind the scene sysadmin stuff.  Trying to figure out the HP situation, sent another email and seems a bit slow. Will keep pinging.
  - Jens: (1) clean-up tooling things and do some exps locally if we can upgrade u-root version and get rid fo some things regarding go modules, (2) gh repo clean-up with linus, (3) review old milestones.

## Decisions

  - Decision: Adopt gitlab conventions:
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/31
  - Decision? What do we do about the hugo theme:
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/36
    - Philipp will give docsy a try and continue this async, to see how complex it is and how our things render.  Too little information for a decision right now.

## Next steps

  - rgdd: mostly Sigsum focus this week; but will fix so that our go unit tests runs in stprov again + add them to our CI.  And let me know if I can unblock you somehow.
  - kai: finish TPM explainer, finish stboot + TPM proposal
  - philipp: finish getting started guide, add more howtos and review/try out the docs from jens. Switching over to a new theme for the documentation ("try"). Getting secure boot docs merged.
  - philipp: go through old meeting notes that linus found, clean-up and make available
  - quite: continue understanding stprov, context netboot. UEFI netboot & secureboot understanding in the pratical usage.
  - nisse: A few smallish MR:s in review.
  - jens: try to finish MR regarding u-root tooling and OS package, ensure there are no open things laying around on my local system when going to OSFC
  - Linus: 
    - figure out what the next ST release should look like <<< will discuss with jens wednesday, rasmus joins too to help move this forward while jens is away
    - hanging around being helpful with whatever you need

## Other

  - osfc
    - jens is having an st workshop
    - fredrik is giving an st talk
  - stprov discussion, in a break-out meeting?
    - (would be nice, with a short break first. +1)
    - same meet room as this meet in 22m (14:10 UTC)
    - [notes here](./2023-10-02-breakout-stprov.md)
