# System Transparency weekly

  - Date:  2023-09-11 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: Fredrik

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - rgdd
  - nisse
  - kai
  - ln5
  - kfreds

## Status rounds

(Mostly "what have I been doing the past week that is important for others to be
aware of")

  - kai, philipp, ln5, rgdd: brief discuss and planning to hash out interfaces
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-09-11-interfaces-discuss.md
 - kai: finished JSON Schema definitions for trust policy, ospkg manifest and
   descriptor:
   https://git.glasklar.is/system-transparency/project/documentation/-/tree/stboot-json.
   Meanwhile I'm discussing the stboot + TPM proposal with nisse.
  - fredrik: giving a talk at osfc!
    - Towards auth and transaprent systems, see description in link below.
    - https://www.osfc.io/2023/talks/towards-authentication-of-transparent-systems/
    - Talk idea: not write a speach, a few slides on different models to think
      about all of this.  Similar talk outline as on PETS which worked great!
  - philipp:
      - Working on the high level interfaces, started documentation as we
        discussed
      - Nearly finished the getting started tutorial with Joel's feedback,
        incoming MR update
      - Finishing some leftovers from before vacation
  - nisse: warmup MRs in ST, getting all tooling installed.  And wrote a
    proposal on how the integration tests could be improved

## Decisions

(In Sigsum we usually have decisions prepared in advance. Example:

```
  - Decision? Something To Be Decided
    - link to proposal
    - scan the room, objections, +1s? are we ready to move forward?
```
)

  - For next time? https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/16
    - Please take a detailed look and provide feedback until next week

## Next steps

(Usually this is "letting other people know what you're going to work on", so
others have a chance to chime in, ask for review, unblock each other. It's also
kind-of "I commit to doing this until next meeting".)

  - rgdd: (still) to get started paging in st
  - kai: define the missing stauth interfaces (measurements, debug_thing).
    Philipp is taking over the stboot interface definitions from me as I think
    it's more efficient if I do the stauth interfaces.
  - philipp:
      - Working on Kai's interface documentation.
      - stauth learning to help Kai with review and feedback.
      - Conference: UAPI specs (UKI, Remote attestation) discussion and feedback
        from ST
  - fredrik: prepare osfc talk/slides
    - Publish model drafts somewhere
  - ln5: 
      - get more gitlab runners going; look into the dreaded gl runner
        ["error 137" failures](https://git.glasklar.is/glasklar/services/gitlab/-/issues/50)
      - get the docs site machinery (not content) into a better shape
      - keep supporting ST with reviews, explanation, discussions
  - nisse: More of the same. Assigned myself on a few ST issues suggested by
    linus.
  - nisse: RE: proposal, want to find out how it works, might make experimental
    MR to see how that goes.  Waiting for comments from jens, and any other
    feedback most welcome.

## Other

(Where we can push stuff that doesn't fit into the categories above, things that
have been pushed from the previous meeting. The open-ended part of the meeting.
Sometimes we also break out here.)

  - Kanban board for "weird" projects and tasks? Examples:
    - HPE machines with open firmware
    - Panel at a conference about software supply chain security
    - Would gl tasks and milestones work for you? See f.ex.
      https://git.glasklar.is/glasklar/ and ask for help with gl in general /ln5
    - We have two ways of doing that now:
      - 1) Issues and milestones in GitLab (can be a private group if necessary)
      - Unless Kanban is required, I think we should express it as issues.
      - 2) Glasklar-internal stuff: In the Glasklar Nextcloud file storage area.
      - We want to gravitate towards open whenever possible
      - Currently have st, sigsum, glasklar areas; all open
      - Request from fredrik: a way to structure project and tasks, and a way to
        store document.  That are not open to the public, but related to st or
        sigsum.
      - Easier to move a private gitlab thing to an open gitlab thing
      - Note: there's also "confidential issues" in gitlab
      - Fredrik will explore gitlab on his own
  - What remained from last week?
    - [Research hardware](https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/11)
      - Idea: we get hardware for research/development, e.g., to do testing and
        quality assurance of our ST project.  And other benefit: we get hardware
        made for open source.  One of the most important things for community:
        we can have certification of recommended hardware for community members.
      - "Test and support ST on this hardware"
      - Brings quality assurance into our hands
      - ln5: would be good for us to have this; price in $$$ is one thing, time
        we need to put in is another.  Might be possible to get going quicker
        than the 40d estimation.
      - fredrik: selecting vendords that have somethign to do with open source
        firmware makes sense, can build relationships with them.  And it helps
        people who want to adopt ST that we can say these vendors and models are
        things that we have tested; sounds good.  So ultimately what we'd use
        them for: "we need to make sure that ST works on some machines".
      - (one x86 machine, one arm machine)
      - fredrik will follow up with ln5/philipp
    - [SecureBoot and signing](https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/12)
      - proposal is about how we deliver a release, specically how to make it
        easier for st adoption.  Systems already come with secureboot.  It's
        hard to enroll your own keys in e.g. your datacenter.  Would be easier
        if we could bring out a default signed release that's accepted.
      - fredrik: generally, think st should support secureboot. The interesting
        question is how, large design space.  Need to understand these options
        more before working towards a decision, but if you have ideas on what
        you think is the right approach -> prototype (?).
  - Use the TPM for stboot's key store:
    https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/10
    - fredrik: same thing here, more info on the design space params needed.
      But if kai/philipp thinks it is the right approach, go ahead and design to
      understand it better.
    - can prototype without the shim signing process, not crucial for the
      understanding
    - (documenting the paths we can go down here would be very valuable; and
      having a repo we do X, stopped at Y, etc., is useful.)
  - stmgr overhaul

## spin-off meeting / breakout

- kfreds, zaolin, kfreds, kai

### boot lab

- x86 *and* arm? 
  - zaolin: we can start with x86 only
  - ln5: arm would be fun but we don't need it now and won't lose anything by
    postponing
- decision: we buy the x86

### SB and shim

(https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/12)

- both Glasklar and other ST users need to understand the design parameter space
  - we should put our knowledge into documentation and code, in order to help
    both our future selfs and potential ST users
- ST need to understand both kfreds design goals and ST user needs

- decision: zaolin and kai spend time this week on writing down the decision
  tree for someone using SB, and add it as a draft to the docs repo

### TPM for stboot

(https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/10)

- decision: everyone read the MR and the comments

### stmgr overhaul

- kai: this is lower priority than SB and TPM+stboot
- decision: iceboxed for now

## other

- kfreds thinks we should *not* continue the shim review process, for two
  reasons:
    - might not be the only path forward
    - seems to be something we can only do once, and therefore we need to get it
      right the first time
