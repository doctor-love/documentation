# System Transparency weekly

  - Date:  2023-02-01 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230201
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Jens
  - Fredrik
  - Foxboron

## Status rounds

  - [Jens] Working on tooling, good progress
  - [Fredrik] Road mapping
  - [Foxboron] Network code refactoring, first take
  - [ln5] Dealing with a MPT bug and road mapping with Fredrik

## Decisions

  - None.

## Next steps

  - [Jens] Finish tooling and import paths work
  - [Jens] Implement the trust policy in stboot
  - [Fredrik] Concretising the road map
  - [Foxboron] Test cases for the networking code and host config
  - [ln5] Road map with Fredrik and actually *fixing* the MPT bug

## Other

  - [Jens] What should we do with ST Github repo's?
    - GH archiving means that a repo is read-only, and you cannot file issues
    - Archiving or not? Yes, we don't want to tend to both places.
    - Mirroring GL -> GH or not? Yes, for discovery.
    - Decision: ln5 is responsible for fixing this, with kind help from Jens.
  - [Fredrik] Road mapping
    - ST background, goals, staff updates and a draft for a road map was presented by Fredrik
    - Discussions will continue in a separate meeting tomorrow 2023-02-02 10:00-11:00 CET, meeting material in https://pad.sigsum.org/p/kmTBOWVDunxkKZ33ajg5

