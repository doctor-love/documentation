# Discussions about UEFI shim signing process

Date and time: 2023-03-13 10:00-10:45 CET
Present: zaolin, ln5

- There's a shim review repository, which we clone and fill in
  - https://github.com/rhboot/shim-review
  - 269 issues processed, have not checked how many of those were approved.
  - Someone's trying to get an iPXE shim signed.

- The process doesn't seem to require an EV cert
  - So let's skip involving a CA.
  - In theory we could probably use a TKEY as an HSM but let's use a
    YubiHSM for now.

- Our plan is to generate keys on a YubiHSM
  - Wrap and export the key for backup purposes.
  - Document the process, both for the shim signing review and for ST
    users to learn from.

- Example of how bad the Secure Boot model is
  - Black Lotus is practically MS signed malware
  - Updating the block list, a CRL called DBX, is in practice not
    happening.
  - Attestation is thus required.

- Next steps
  - zaolin clones the shim review repo and fill it in, adding
    documentation about the HTTP boot process to the repo in markdown
    format.
  - ln5 deals with the improper packaging of the two YubiHSM's shipped
    last week.
