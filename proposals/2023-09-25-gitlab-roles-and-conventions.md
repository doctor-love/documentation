# Proposal on gitlab conventions and the assignee / reviewer roles

The objective of this proposal is to agree, clarify and document how
we collaborate on gitlab, e.g., answering the question "who's expected
to click that merge button".

## Previous discussion

Gitlab conventions in Sigsum are documented here:
https://git.glasklar.is/sigsum/project/documentation/-/blob/main/archive/2023-09-05-sigsum-processes.md?ref_type=heads#collaborating-in-gitlab,
I think the section on gitlab MR:s could be applied as is also for ST
(while Sigsum's process for project planning and roadmapping would
need further discussion before adopting for ST).

## Assignee/reviewer roles

### Assignee role

Each MR should have an assignee, usually the person who created the MR
in the first place. The assignee is responsible for driving the MR
forward, until it is eventually merged (or closed, in case it turns
out it shouldn't be merged). When the MR is ready for review, the
assignee selects a reviewer, and work with the reviewer until both are
happy with the MR.

In case the assignee lacks merge privileges when the MR is about to be
merged, assignee needs to coordinate with a maintainer and reassign
the MR to someone who can take over responsibility for getting it
merged.

### Reviewer role

Reviewer is expected to review, propose MR improvements as well as
followup work, and pressing approval button when happy with merging of
the MR (to reduce review roundtrips, can also do conditional approve:
press the approve button, but also write a comment "I approve MR
modulo the following issues that I think should be fixed in this
way").

## MR Discussions

It can be a bit difficult to navigate MR discussion threads. Resolving
threads helps highlighting remaining issues, since resolved threads
are mostly hidden by the ui. Reducing the amount of needed discussion
is one reason to keep MRs small.

### Resolving threads

There are different flavors of MR threads, e.g., a thread can be a
suggestion for improvement, a question on how the code (old or new)
works, or a tangential discussion. So there are many ways a thread can
be resolved: Changes are made to the MR (usually by the assignee, per
above), a question is answered, or an additional MR or issue is
created for a concern that is out of scope for current MR.

A thread can be marked resolved in the UI, when the thread is
resolved. One can add a comment like "fixed", "done", "filed issue
..." when resolving. That provides others with a notification, and the
thread can be unresolved if they unexpectedly have further comments.

Marking threads as resolved is the responsibility of the assignee.
Others may mark threads as resolved, if they are confident that is
appropriate and helpful for the assignee.

When in doubt, leave open and ask, e.g., "fixed by doing X, please
resolve if you think that is good enough". If resolving a thread when
answering a question, there's a risk that the answer will be
overlooked, so this should perhaps be avoided.

### Tangential discussions

It's easy to bring up tangential discussions in MR threads. Usually,
one should not expand the scope of an MR to solve additional problems.
Valid but out-of-scope suggestions should be noted as followup work,
which can be addressed either right away in a separate MR, or have an
issue filed for tracking later work.

### Out-of-band discussions

When issues on an MR are hashed out in a call or face-to-face meeting,
please summarize the conclusions in an MR comment or linked issue.

### Additional reviewers

Gitlab only supports one reviewer, for getting a second opinion, can
tag other people with `@user` and ask for feedback. Please be explicit
about what question or which aspect of the MR you want feedback on.
Such additional reviewers are expected to provide feedback in MR
comments. Reviewing the MR as a whole is encouraged but optional.
Approval from an additional reviewer can be interpreted as approval of
the MR as a whole.

### Wider review

In case wider review of an MR is desired, bring attention to it via
the #system-transparency room on matrix and/or ST weekly meeting, or
call an adhoc meeting with relevant people. Make clear what is needed
to make progress, and why it's debatable whether or not the MR should
be merged in its current form.

If you find yourself in this spot, that might be a sign that you
should write a proposal, get feedback, and bring for a decision on ST
weekly. Once there's agreement on what the change should do, review
and merge of the implementation should be more straight-forward.
