# QA & Research (Open Source) Hardware

I would like to bring the topic to discussion about getting OSF hardware into our lab for research, development & QA.



## Goals

1. Research/development hardware with interesting features
   
   1. TEE/Confidential Computing
   
   2. Open Source Firmware (OpenBMC, coreboot, LinuxBoot)

2. QA and testing automation hardware (Gitlab CI)

3. Good connection to hardware vendors being early adoptors

4. Project certification of recommended hardware for community members

## Risks

We tend to spend some time helping them to understand what we need and would also need to give some feedback after evaluation. Overall time invest might be **40** **PD** including getting hardware into the rack functional.

There might be additional time invest for getting the QA running for ST on these machines but we can't rely on community members anymore for testing and need to do our own evaluation/release/QA cycle.

## HPE Systems

We got in contact with the head of firmware development at HPE. They offer at the moment all middle class servers like ProLiant DL systems with OpenBMC and would also make us early adopters for their LinuxBoot integration. Open Source Host firmware is part of the roadmap next year (coreboot, tianocore). The support and responsiveness is extreme nice and helpful. 

## Supermicro Systems

We got in contact during Coscup in Taipeh with the Product Management. We talked a bit about making low cost devices like the X11 available with OpenBMC, LinuxBoot support and maybe coreboot integration. They are already providing OSF hardware (Eagle Stream) high class Intel servers to customers. They come with OpenBMC, coreboot & LinuxBoot.

## Status & Roadmap

- [x] Get in contact with HPE

- [x] Get in contact with Supermicro

- [x] Specify requirements for the hardware for HPE

- [ ] Specify requirements for the hardware for Supermicro

- [x] Get Quote for specific systems from HPE

- [ ] Get Quote for specific systems from Supermicro

- [ ] Decision to be made if we want to have the hardware or not
  
  - [ ] HPE
  
  - [ ] Supermicro
