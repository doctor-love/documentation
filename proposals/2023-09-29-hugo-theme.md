# Hugo Theme

**Author**: Philipp Deppenwiese

## Objective

Others and I have experienced issues with the theme we currently use as part of our hugo documentation base:

* The theme  "shadocs" is hard to customize, e.g. it lacks customization option.
* Is overloaded with features which we can't disable without touching the theme itself.
* Zooms/scales incorrectly for images / mermaid.
* Versioning support isn't based on github tags instead on directories.
* Missing documentation and some features are buggy.

Instead of wasting the time working on a non-functional theme, I would like to choose another theme which is stable, supports versioning and offers plenty of customization.

## Why do we care?

Since we provide the documentation for outsiders and we want them to read it in a way where they can easily utilize the documentation, a good customized theme is mandatory.

Versioning is important for us to be based on tags so we can provide documentation for interfaces and data structures when we break the API or format.

## Potential options

**Demo:** https://docs.edgeless.systems/constellation/

**Static Page Generator:** Jekyll

Used by Edgeless for their product documentation, super slick and simple. Just for documentation.



**Demo:** https://www.docsy.dev/

**Static Page Generator:** Hugo

Developed & maintained by Google, comes with a lot customization. Also offers all-in-one homepage feature.

## Proposal

I recommend to change the theme to "docsy" theme because staying with hugo makes it easier for us.  There are more documentation themes for sure but either they are missing versioning or not being maintained well enough.
