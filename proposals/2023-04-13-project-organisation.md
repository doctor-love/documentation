# ST project organisation

How we structure our work on and make releases of ST code and
documentation.

This proposal is about how the ST project builds and releases the
artefacts that comprise System Transparency. An important
consideration is the way ST users are supposed to install and upgrade parts of
ST with the least amount of fear and agony.

For an overview of the artefacts produced by ST, see the
[road map document](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-04-13-draft-roadmap-draft.md).

## Users

ST has two types of users.

First the conventional type of user who consumes the output from the
project, the **consuming user**. They retrieve code and documentation and
use it. Operations teams and package maintainers are examples of
consuming users. They should be provided with a well defined set of
artefacts with version numbers following the [semantic versioning
scheme](https://en.wikipedia.org/wiki/Software_versioning#Semantic_versioning).
Compatibility between older and newer versions of programs must be
well tested and clearly communicated to users.

The other type of user is the **developing user**. They use code and
documentation from the ST project to build their own tools or
products. They are expected to know more about what's happening within
the project and be able to pick and choose more freely than what
consumers do.


## Source repositories and releases

ST produces code and documentation in multiple git repositories. Each
repository contains a single ST **artefact** that can be tagged and released
separately from any other artefact. Dependencies between artefacts
sometimes makes it necessary to coordinate artefact releases.

An artefact is released by creating an **annotated tag** in its git
repository and optionally publishing a signed **release tar ball** with the
contents of the repo. A release should contain a **release note**
explaining what has changed since the previous release. A release may
come with a **release announcement** suitable for being pushed to mailing
lists and other communications channels.


### Background

TODO flesh out

- we use separate repos, ie not taking a monorepo approach
- this in order to have separate version numbers, because
  - ST is a diverse beast and will release specifications and a boot
    loader and tools and documentation and libraries in multiple
    languages and whatnot
  - we must not assume that it's convenient or even possible for a
    user to upgrade *all* of ST at once
    - so there's a value for users responsible for running ST artefact
      FOO to not have to care so much about releases of ST artefacts
      BAR and BAZ
    - case in point: stable stboot, moving tooling
  - there's also a value in limiting the explosion of combinations of
    versions of artefacts to test with each other
- note to golang devs: we assume one repo, one go module bc BCP says so


## ST deliverables

ST publishes code and documentation in **ST release tar balls** containing
compatible versions of ST artefact releases.

In addition to release tar balls there might be **ST nightly tar balls** with
certain configurations of released and unreleased artefacts, meant for
testing.

The documentation artefact is being released as part of ST release tar balls
and is also published to https://docs.system-tranparency.org/.


## Testing

TODO describe automatic testing machinery guaranteeing compatibility verification
